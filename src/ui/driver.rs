use crate::handlers::chat::Chat;
use crate::ui::keybinds::DriverAction;
use crate::{handlers::chat::View, settings::Settings};
use anyhow::Result;
use crossterm::terminal::enable_raw_mode;
use crossterm::{
    event::{self, DisableMouseCapture, EnableMouseCapture, Event as CrosstermEvent},
    execute,
    terminal::{disable_raw_mode, EnterAlternateScreen, LeaveAlternateScreen},
};
use futures::StreamExt;
use std::sync::Arc;
use std::{
    io::{self, Stdout},
    time::Duration,
};
use tokio::sync::mpsc::{UnboundedReceiver, UnboundedSender};
use tokio::sync::Semaphore;
use tui::widgets::{Row, Table};
use tui::{
    backend::CrosstermBackend,
    layout::{Alignment, Constraint, Corner, Direction, Layout},
    style::{Color, Modifier, Style},
    widgets::{Block, BorderType, Borders, List, ListItem, ListState, Paragraph},
    Frame, Terminal,
};
use twitch_irc::SecureTCPTransport;
use twitch_irc::{login::StaticLoginCredentials, message::ServerMessage, TwitchIRCClient};

static TICK_RATE: Duration = Duration::from_millis(100);

pub enum Event {
    Input(CrosstermEvent),
    ChatMessage(Box<ServerMessage>),
    Tick,
}

async fn feed_input(evt_tx: UnboundedSender<Event>) {
    let mut stream = event::EventStream::new().fuse();
    while let Some(input) = stream.next().await {
        if let Ok(input) = input {
            if let Err(_) = evt_tx.send(Event::Input(input)) {
                // the receiver was dropped, we don't need to pump events anymore
                break;
            }
        }
        // ignore the error for now
    }
}

fn try_feed_tick(semaphore: Arc<Semaphore>, evt_tx: &UnboundedSender<Event>) {
    if let Ok(permit) = semaphore.try_acquire_owned() {
        evt_tx.send(Event::Tick).unwrap_or(());
        tokio::spawn(async move {
            tokio::time::sleep(TICK_RATE).await;
            drop(permit);
        });
    }
}

pub async fn run(
    settings: Settings,
    evt_tx: UnboundedSender<Event>,
    mut evt_rx: UnboundedReceiver<Event>,
    mut msg_tx: UnboundedSender<(String, String)>,
    rx_client: TwitchIRCClient<SecureTCPTransport, StaticLoginCredentials>,
) {
    enable_raw_mode().unwrap();
    let mut stdout = io::stdout();
    execute!(stdout, EnterAlternateScreen, EnableMouseCapture).unwrap();
    let backend = CrosstermBackend::new(stdout);
    let mut terminal = Terminal::new(backend).unwrap();
    terminal.clear().unwrap();

    // send an initial tick to draw the initial ui
    evt_tx.send(Event::Tick).unwrap_or(());

    tokio::task::spawn(feed_input(evt_tx.clone()));

    let mut chat = Chat::new(settings, rx_client).await;

    let mut chatter_list_state = ListState::default();
    chatter_list_state.select(Some(0));

    let mut blocklist_state = ListState::default();
    blocklist_state.select(Some(0));

    // This is used to throttle updates caused by many chat messages
    let message_semaphore = Arc::new(Semaphore::new(1));

    // We use the `evt_tx` handle to feed ourself with updates.
    loop {
        match evt_rx.recv().await.unwrap() {
            Event::ChatMessage(msg) => {
                chat.handle_message(msg).unwrap();
                try_feed_tick(message_semaphore.clone(), &evt_tx);
            }
            Event::Input(event) => {
                if let CrosstermEvent::Key(key) = event {
                    let action = match chat.view {
                        View::Normal => super::keybinds::normal_mode(&mut chat, key, &mut msg_tx),
                        View::Chatters => {
                            super::keybinds::chatters_mode(&mut chat, key, &mut chatter_list_state)
                        }
                        View::BlockWord => {
                            super::keybinds::block_word_mode(&mut chat, key, &mut blocklist_state)
                        }
                        View::ChangeChannel => {
                            super::keybinds::change_channel_mode(&mut chat, key).await
                        }
                        View::Help => super::keybinds::help_mode(&mut chat, key),
                        _ => DriverAction::None,
                    };
                    match action {
                        DriverAction::Quit => break,
                        DriverAction::Redraw => evt_tx.send(Event::Tick).unwrap_or(()),
                        DriverAction::None => (),
                    }
                }
            }
            Event::Tick => {
                terminal
                    .draw(|rect| {
                        chat.win_width = rect.size().width as usize;
                        match chat.view {
                            View::Normal => {
                                draw_normal_view(rect, &chat).unwrap();
                            }
                            View::Chatters => {
                                draw_chatters_view(rect, &chat, &mut chatter_list_state).unwrap();
                            }
                            View::BlockWord => {
                                draw_blocklist_view(rect, &chat, &mut blocklist_state).unwrap();
                            }
                            View::ChangeChannel => {
                                draw_change_channel_view(rect, &chat).unwrap();
                            }
                            View::Help => {
                                draw_help_view(rect).unwrap();
                            }
                            _ => {}
                        }
                    })
                    .unwrap();
            }
        }
    }
    disable_raw_mode().unwrap();
    execute!(
        terminal.backend_mut(),
        LeaveAlternateScreen,
        DisableMouseCapture
    )
    .unwrap();
    terminal.show_cursor().unwrap();
    std::process::exit(0);
}

pub fn draw_normal_view(rect: &mut Frame<CrosstermBackend<Stdout>>, chat: &Chat) -> Result<()> {
    let size = rect.size();
    let chunks = Layout::default()
        .direction(Direction::Vertical)
        .constraints([Constraint::Min(3), Constraint::Length(3)].as_ref())
        .split(size);
    let chat_title = format!("[ {} ]", &chat.settings.current_channel());
    let chat_block = Block::default()
        .borders(Borders::ALL)
        .style(Style::default().fg(Color::White))
        .title(chat_title.as_ref())
        .border_style(Style::default().fg(Color::Rgb(170, 170, 170)))
        .border_type(BorderType::Plain);

    let list = List::new(chat.messages.clone())
        .block(chat_block)
        .start_corner(Corner::BottomLeft);
    rect.render_widget(list, chunks[0]);

    let input_title = format!("Send a message as {}", &chat.settings.user_name());

    let width = chunks[1].width.max(3) - 3;
    let scroll = (chat.input.cursor() as u16).max(width) - width;
    let input = Paragraph::new(chat.input.value())
        .style(Style::default().fg(Color::White))
        .scroll((0, scroll))
        .block(
            Block::default()
                .borders(Borders::ALL)
                .border_style(Style::default().fg(Color::Rgb(170, 170, 170)))
                .title(input_title),
        );

    rect.set_cursor(
        chunks[1].x + (chat.input.cursor() as u16).min(width) + 1,
        chunks[1].y + 1,
    );

    rect.render_widget(input, chunks[1]);
    Ok(())
}

pub fn draw_chatters_view(
    rect: &mut Frame<CrosstermBackend<Stdout>>,
    chat: &Chat,
    cls: &mut ListState,
) -> Result<()> {
    let size = rect.size();
    let chunks = Layout::default()
        .direction(Direction::Vertical)
        .constraints([Constraint::Percentage(100)].as_ref())
        .split(size);

    let title = format!("[ Chatters in {} ]", &chat.settings.current_channel());
    let chatters_block = Block::default()
        .borders(Borders::ALL)
        .style(Style::default().fg(Color::White))
        .title(title.as_ref())
        .border_style(Style::default().fg(Color::Rgb(170, 170, 170)))
        .border_type(BorderType::Plain);

    let mut items: Vec<ListItem> = Vec::new();
    for (_, c_item) in chat.chatters.iter() {
        items.push(c_item.clone());
    }

    let list = List::new(items)
        .block(chatters_block)
        .highlight_style(Style::default().add_modifier(Modifier::REVERSED));
    rect.render_stateful_widget(list, chunks[0], cls);
    Ok(())
}

fn draw_blocklist_view(
    rect: &mut Frame<CrosstermBackend<Stdout>>,
    chat: &Chat,
    bls: &mut ListState,
) -> Result<()> {
    let size = rect.size();
    let chunks = Layout::default()
        .direction(Direction::Vertical)
        .constraints([Constraint::Min(3), Constraint::Length(3)].as_ref())
        .split(size);
    let chat_block = Block::default()
        .borders(Borders::ALL)
        .style(Style::default().fg(Color::White))
        .title("[ Blocked words ]")
        .border_style(Style::default().fg(Color::Rgb(170, 170, 170)))
        .border_type(BorderType::Plain);

    let mut items: Vec<ListItem> = Vec::new();
    for (_, b_item) in chat.blocklist.iter() {
        items.push(b_item.clone());
    }

    let list = List::new(items)
        .block(chat_block)
        .highlight_style(Style::default().add_modifier(Modifier::REVERSED));
    rect.render_stateful_widget(list, chunks[0], bls);

    let width = chunks[1].width.max(3) - 3;
    let scroll = (chat.input.cursor() as u16).max(width) - width;
    let input = Paragraph::new(chat.input.value())
        .style(Style::default().fg(Color::White))
        .scroll((0, scroll))
        .block(
            Block::default()
                .borders(Borders::ALL)
                .border_style(Style::default().fg(Color::Rgb(170, 170, 170)))
                .title("Enter a word to block"),
        );

    rect.set_cursor(
        chunks[1].x + (chat.input.cursor() as u16).min(width) + 1,
        chunks[1].y + 1,
    );

    rect.render_widget(input, chunks[1]);
    Ok(())
}

fn draw_change_channel_view(rect: &mut Frame<CrosstermBackend<Stdout>>, chat: &Chat) -> Result<()> {
    let size = rect.size();
    let chunks = Layout::default()
        .direction(Direction::Vertical)
        .constraints(
            [
                Constraint::Percentage(40),
                Constraint::Length(3),
                Constraint::Length(3),
                Constraint::Percentage(40),
            ]
            .as_ref(),
        )
        .split(size);

    let current_channel = format!("Current channel: {}", &chat.settings.current_channel());
    let current_channel_p = Paragraph::new(current_channel)
        .block(Block::default().borders(Borders::NONE))
        .alignment(Alignment::Center);

    rect.render_widget(current_channel_p, chunks[1]);

    let width = chunks[1].width.max(3) - 3;
    let scroll = (chat.input.cursor() as u16).max(width) - width;
    let input = Paragraph::new(chat.input.value())
        .style(Style::default().fg(Color::White))
        .scroll((0, scroll))
        .block(
            Block::default()
                .borders(Borders::ALL)
                .border_style(Style::default().fg(Color::Rgb(170, 170, 170)))
                .title("Enter a channel name"),
        );

    rect.set_cursor(
        chunks[2].x + (chat.input.cursor() as u16).min(width) + 1,
        chunks[2].y + 1,
    );
    rect.render_widget(input, chunks[2]);
    Ok(())
}

fn draw_help_view(rect: &mut Frame<CrosstermBackend<Stdout>>) -> Result<()> {
    let size = rect.size();

    let chunks = Layout::default()
        .direction(Direction::Vertical)
        .constraints(vec![Constraint::Min(10)].as_ref())
        .vertical_margin(15)
        .split(size);

    let block = Block::default()
        .title("[ Help ]")
        .borders(Borders::ALL)
        .border_style(Style::default().fg(Color::Rgb(170, 170, 170)));

    let table = Table::new(vec![
        Row::new(vec!["Ctrl+q", "Exit the application."]),
        Row::new(vec!["Ctrl+c", "Change channel."]),
        Row::new(vec!["Ctrl+v", "Chatter list."]),
        Row::new(vec!["Ctrl+b", "Blocked words list."]),
        Row::new(vec!["Ctrl+n", "Normal view."]),
        Row::new(vec!["Ctrl+h", "Help."]),
        Row::new(vec!["Esc", "Exit alternate view."]),
    ])
    .header(Row::new(vec!["Keybind", "Description"]).style(Style::default().fg(Color::Blue)))
    .widths(&[Constraint::Percentage(25), Constraint::Percentage(75)])
    .style(Style::default().fg(Color::White))
    .block(block);

    rect.render_widget(table, chunks[0]);

    Ok(())
}
