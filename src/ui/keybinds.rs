use crate::handlers::chat::{Chat, View};
use crossterm::event::{Event as CrosstermEvent, KeyCode, KeyEvent, KeyModifiers};
use tokio::sync::mpsc::UnboundedSender;
use tui::widgets::ListState;
use tui_input::backend::crossterm as input_backend;

pub enum DriverAction {
    Quit,
    Redraw,
    None,
}

pub fn normal_mode(
    chat: &mut Chat,
    key: KeyEvent,
    msg_tx: &mut UnboundedSender<(String, String)>,
) -> DriverAction {
    match key.modifiers {
        KeyModifiers::CONTROL => {
            match key.code {
                KeyCode::Char('q') => return DriverAction::Quit,
                KeyCode::Char('v') => chat.view = View::Chatters,
                KeyCode::Char('b') => chat.view = View::BlockWord,
                KeyCode::Char('c') => chat.view = View::ChangeChannel,
                KeyCode::Char('h') => chat.view = View::Help,
                _ => return DriverAction::None,
            };
            DriverAction::Redraw
        }
        KeyModifiers::NONE | KeyModifiers::SHIFT => {
            match key.code {
                KeyCode::Enter => {
                    let msg: String = (*chat.input.value()).into();
                    chat.message_history.push(msg.to_owned());
                    chat.input = tui_input::Input::default();
                    if let Err(err) = msg_tx.send((chat.settings.current_channel(), msg)) {
                        panic!("{}", err);
                    }
                }
                KeyCode::Tab => {
                    let input: String = (*chat.input.value()).into();
                    let input_split: Vec<&str> = input.split(' ').collect();
                    let search_term = input_split[input_split.len() - 1];
                    for (chatter, _) in chat.chatters.iter() {
                        if chatter.starts_with(search_term) && !chatter.starts_with("+") {
                            let input = if input_split.len() > 1 {
                                let prev_items: &[&str] = &input_split[..input_split.len() - 1];
                                let new_input = format!("{} {} ", prev_items.join(" "), chatter);
                                new_input
                            } else {
                                chatter.to_owned() + ", "
                            };
                            chat.input = tui_input::Input::default().with_value(input);
                            return DriverAction::Redraw;
                        }
                    }
                }
                KeyCode::Up => {
                    chat.input =
                        tui_input::Input::default().with_value(chat.message_history.previous());
                }
                KeyCode::Down => {
                    chat.input =
                        tui_input::Input::default().with_value(chat.message_history.next());
                }
                _ => {
                    let _ = input_backend::to_input_request(CrosstermEvent::Key(key))
                        .and_then(|req| chat.input.handle(req));
                }
            }
            DriverAction::Redraw
        }
        KeyModifiers::ALT if key.code == KeyCode::Enter => {
            // CTRL+Enter sends the messages but doesnt clear input
            let msg: String = (*chat.input.value()).into();
            chat.message_history.push(msg.to_owned());
            if let Err(err) = msg_tx.send((chat.settings.current_channel(), msg)) {
                panic!("{}", err);
            }
            // doesn't need to redraw since nothing changed
            DriverAction::None
        }
        _ => DriverAction::None,
    }
}

pub fn chatters_mode<'a>(chat: &mut Chat, key: KeyEvent, cls: &mut ListState) -> DriverAction {
    let decrement = |step: usize, sel: Option<usize>| {
        if let Some(selected) = sel {
            let amount = &chat.chatters.len();
            return if selected > step {
                let mut next = selected - step;
                let (i_name, _) = chat.chatters.get(next).unwrap();
                if i_name.starts_with("+h") {
                    next -= 2;
                } else if i_name.starts_with("+e") {
                    next -= 1;
                }
                Some(next)
            } else {
                Some(amount - 1)
            };
        }
        None
    };

    let increment = |step: usize, sel: Option<usize>| {
        if let Some(selected) = sel {
            let amount = &chat.chatters.len();
            return if selected.ge(&(amount - step)) {
                Some(0)
            } else {
                // we do these checks to jump over selecting headers and empty lines
                let mut next = selected + step;
                let (i_name, _) = chat.chatters.get(next).unwrap();
                if i_name.starts_with("+e") {
                    next += 2;
                } else if i_name.starts_with("+h") {
                    next += 1;
                }
                Some(next)
            };
        }
        None
    };
    match key.modifiers {
        KeyModifiers::CONTROL => match key.code {
            KeyCode::Up => {
                cls.select(decrement(10, cls.selected()));
                DriverAction::Redraw
            }
            KeyCode::Down => {
                cls.select(increment(10, cls.selected()));
                DriverAction::Redraw
            }
            KeyCode::Char('q') => DriverAction::Quit,
            KeyCode::Char('v') | KeyCode::Char('n') => {
                chat.view = View::Normal;
                DriverAction::Redraw
            }
            _ => DriverAction::None,
        },
        KeyModifiers::NONE => match key.code {
            KeyCode::Up => {
                cls.select(decrement(1, cls.selected()));
                DriverAction::Redraw
            }
            KeyCode::Down => {
                cls.select(increment(1, cls.selected()));
                DriverAction::Redraw
            }
            KeyCode::Esc => {
                chat.view = View::Normal;
                DriverAction::Redraw
            }
            _ => DriverAction::None,
        },
        _ => DriverAction::None,
    }
}

pub fn block_word_mode(chat: &mut Chat, key: KeyEvent, bls: &mut ListState) -> DriverAction {
    match key.modifiers {
        KeyModifiers::CONTROL => match key.code {
            KeyCode::Char('q') => DriverAction::Quit,
            KeyCode::Char('b') | KeyCode::Char('n') => {
                chat.view = View::Normal;
                DriverAction::Redraw
            }
            KeyCode::Char('d') => {
                let selected = bls.selected().unwrap();
                chat.blocklist.remove(selected);
                if selected > 0 {
                    bls.select(Some(selected - 1));
                }
                DriverAction::Redraw
            }
            _ => DriverAction::None,
        },
        KeyModifiers::NONE => {
            match key.code {
                KeyCode::Up => {
                    if let Some(selected) = bls.selected() {
                        let amount = chat.blocklist.len();
                        if selected.ge(&(amount - 1)) {
                            bls.select(Some(0))
                        } else {
                            bls.select(Some(selected + 1))
                        }
                    }
                }
                KeyCode::Down => {
                    if let Some(selected) = bls.selected() {
                        let amount = chat.blocklist.len();
                        if selected > 0 {
                            bls.select(Some(selected - 1))
                        } else {
                            bls.select(Some(amount - 1))
                        }
                    }
                }
                KeyCode::Enter => {
                    let msg: String = (*chat.input.value()).into();
                    chat.message_history.push(msg.to_owned());
                    chat.input = tui_input::Input::default();
                    chat.add_blocked_word(msg);
                }
                KeyCode::Esc => {
                    chat.view = View::Normal;
                }
                _ => {
                    let _ = input_backend::to_input_request(CrosstermEvent::Key(key))
                        .and_then(|req| chat.input.handle(req));
                }
            }
            DriverAction::Redraw
        }
        _ => DriverAction::None,
    }
}

pub async fn change_channel_mode(chat: &mut Chat, key: KeyEvent) -> DriverAction {
    match key.modifiers {
        KeyModifiers::CONTROL => match key.code {
            KeyCode::Char('q') => DriverAction::Quit,
            KeyCode::Char('c') | KeyCode::Char('n') => {
                chat.view = View::Normal;
                DriverAction::Redraw
            }
            _ => DriverAction::None,
        },
        KeyModifiers::NONE | KeyModifiers::SHIFT => {
            match key.code {
                KeyCode::Enter => {
                    let new_channel: String = (*chat.input.value()).into();
                    chat.change_channel(new_channel).await.unwrap();
                    chat.input = tui_input::Input::default();
                    chat.view = View::Normal;
                }
                KeyCode::Esc => {
                    chat.view = View::Normal;
                }
                _ => {
                    let _ = input_backend::to_input_request(CrosstermEvent::Key(key))
                        .and_then(|req| chat.input.handle(req));
                }
            }
            DriverAction::Redraw
        }
        _ => DriverAction::None,
    }
}

pub fn help_mode(chat: &mut Chat, key: KeyEvent) -> DriverAction {
    match key.modifiers {
        KeyModifiers::CONTROL => match key.code {
            KeyCode::Char('q') => DriverAction::Quit,
            KeyCode::Char('h') | KeyCode::Char('n') => {
                chat.view = View::Normal;
                DriverAction::Redraw
            }
            _ => DriverAction::None,
        },
        KeyModifiers::NONE if key.code == KeyCode::Esc => {
            chat.view = View::Normal;
            DriverAction::Redraw
        }
        _ => DriverAction::None,
    }
}
