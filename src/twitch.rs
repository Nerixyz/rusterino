use std::collections::HashMap;

use crate::settings::User;
use anyhow::Result;
use serde_json::map::Map;
use serde_json::Value;
use twitch_irc::login::StaticLoginCredentials;
use twitch_irc::ClientConfig;

pub fn config(user: &User) -> Result<ClientConfig<StaticLoginCredentials>> {
    Ok(ClientConfig::new_simple(StaticLoginCredentials::new(
        user.name.to_owned(),
        Some(user.oauth_token.to_owned()),
    )))
}

pub async fn chatters(channel: &str) -> Result<(u64, Map<String, Value>)> {
    let uri = format!("https://tmi.twitch.tv/group/user/{}/chatters", channel);
    let chatters_object: HashMap<String, Value> = reqwest::get(uri).await?.json().await?;

    let count = chatters_object["chatter_count"]
        .as_u64()
        .unwrap_or_default();
    let chatters: Map<String, Value> = chatters_object["chatters"].as_object().unwrap().to_owned();
    Ok((count, chatters))
}
