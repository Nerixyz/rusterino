mod auth;
mod handlers;
mod settings;
mod twitch;
mod ui;
mod util;

use anyhow::Result;
use settings::Settings;
use tokio::sync::mpsc;
use twitch_irc::login::StaticLoginCredentials;
use twitch_irc::{SecureTCPTransport, TwitchIRCClient};
use ui::driver::Event;

#[actix_web::main]
async fn main() -> Result<()> {
    // Read settings
    let settings = Settings::read()?;
    let user = settings.user();

    // event handler (tick, keypress, incoming chat msg)
    let (evt_tx, evt_rx) = mpsc::unbounded_channel::<Event>();
    // sending message from the input box to the irc client
    let (ui_msg_tx, mut ui_msg_rx) = mpsc::unbounded_channel::<(String, String)>();

    let tx_config = twitch::config(&user)?;
    let rx_config = twitch::config(&user)?;

    // we define two clients, one for reading the chat and one for sending messages
    // the tx client doesnt have to connect to the chatroom
    let (_, tx_client) =
        TwitchIRCClient::<SecureTCPTransport, StaticLoginCredentials>::new(tx_config);
    let (mut incoming_messages, rx_client) =
        TwitchIRCClient::<SecureTCPTransport, StaticLoginCredentials>::new(rx_config);

    let evt_tx2 = evt_tx.clone();
    let join_handle = tokio::spawn(async move {
        while let Some(message) = incoming_messages.recv().await {
            if let Err(err) = evt_tx2.send(Event::ChatMessage(Box::new(message))) {
                eprintln!("{}", err);
                return;
            }
        }
    });

    tokio::spawn(async move {
        while let Some((channel, msg)) = ui_msg_rx.recv().await {
            tx_client.privmsg(channel, msg).await.unwrap();
        }
    });

    tokio::spawn(async move {
        join_handle.await.unwrap();
    });

    ui::driver::run(settings, evt_tx, evt_rx, ui_msg_tx, rx_client).await;

    Ok(())
}
