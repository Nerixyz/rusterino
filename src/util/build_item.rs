use textwrap::wrap;
use tui::{
    style::{Color, Modifier, Style},
    text::{Span, Spans, Text},
    widgets::ListItem,
};

pub fn header(name: String) -> ListItem<'static> {
    ListItem::new(Span::styled(name, Style::default().fg(Color::Blue)))
}

pub fn entry(name: String) -> ListItem<'static> {
    ListItem::new(Span::styled(name, Style::default().fg(Color::White)))
}

pub fn message(
    time_stamp: Span<'static>,
    badges: Option<String>,
    sender: Option<String>,
    content: String,
    splitter: Option<String>,
    max_content_width: usize,
    name_style: Option<Style>,
    text_style: Style,
) -> ListItem<'static> {
    let mut text: Text = Text::from("");
    let mut formatted = Spans(vec![
        time_stamp,
        Span::raw(badges.unwrap_or_default()),
        Span::styled(
            sender.unwrap_or_default(),
            name_style.unwrap_or_default().add_modifier(Modifier::BOLD),
        ),
        Span::styled(splitter.unwrap_or_default(), name_style.unwrap_or_default()),
    ]);

    let dummy_content = ".".repeat(formatted.width());

    let pre_wrap_content = format!("{}{}", &dummy_content, &content);

    let wrapped_content = wrap(&pre_wrap_content, max_content_width);
    let mut is_first_line = true;
    for line in wrapped_content.iter() {
        let line = line.replace(&dummy_content, "");
        if is_first_line {
            formatted.0.push(Span::styled(line, text_style));
            text.lines.push(formatted.clone());
            is_first_line = false;
        } else {
            text.lines.push(Spans(vec![Span::styled(line, text_style)]));
        }
    }

    ListItem::new(text)
}
