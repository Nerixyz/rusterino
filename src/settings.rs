use crate::auth::auth_page;
use anyhow::Result;
use home::home_dir;
use serde::{Deserialize, Serialize};
use std::fs;

#[derive(Serialize, Deserialize, Clone)]
pub struct User {
    pub name: String,
    pub id: i32,
    pub client_id: String,
    pub oauth_token: String,
}

#[derive(Serialize, Deserialize, Clone)]
pub struct Settings {
    current_channel: String,
    blocked_words: Option<Vec<String>>,
    user: User,
}

impl Drop for Settings {
    fn drop(&mut self) {
        self.save().unwrap();
    }
}

impl Settings {
    pub fn read() -> Result<Settings> {
        let mut config_path = home_dir().unwrap();
        config_path.push(".config/rusterino/");
        if !config_path.exists() {
            fs::create_dir(&config_path)?;
        }
        config_path.push("settings.toml");
        if !config_path.exists() {
            fs::File::create(&config_path)?;
        }
        let content = fs::read_to_string(&config_path)?;
        let settings: Settings = match toml::from_str(&content) {
            Ok(s) => s,
            Err(_) => {
                let user_string = auth_page::open()?;
                let user_split: Vec<&str> = user_string.split(';').collect();

                let name = user_split[0].to_string();

                let user = User {
                    name: name.clone(),
                    id: user_split[1].to_string().parse::<i32>().unwrap(),
                    client_id: user_split[2].to_string(),
                    oauth_token: user_split[3].to_string(),
                };

                let new_settings = Settings {
                    current_channel: name, //user starts in their own channel
                    blocked_words: Some(Vec::new()),
                    user,
                };
                new_settings.save()?;
                new_settings
            }
        };
        Ok(settings)
    }

    pub fn _update_user(mut self) -> Result<()> {
        let auth_string = auth_page::open()?;

        let user_split: Vec<&str> = auth_string.split(';').collect();
        let user = User {
            name: user_split[0].to_string(),
            id: user_split[1].to_string().parse::<i32>().unwrap(),
            client_id: user_split[2].to_string(),
            oauth_token: user_split[3].to_string(),
        };
        self.user = user;
        Ok(())
    }

    pub fn user(&self) -> User {
        self.user.clone()
    }

    pub fn current_channel(&self) -> String {
        self.current_channel.to_string()
    }

    pub fn change_channel(&mut self, new_channel: String) {
        self.current_channel = new_channel;
        self.save().unwrap();
    }

    pub fn user_name(&self) -> String {
        self.user.name.to_string()
    }

    pub fn blocked_words(&self) -> Option<Vec<String>> {
        self.blocked_words.clone()
    }

    pub fn add_blocked_word(&mut self, word: String) {
        let mut bw = self.blocked_words.clone().unwrap_or_default();
        bw.push(word);
        self.blocked_words = Some(bw);
        self.save().unwrap();
    }

    pub fn save(&self) -> Result<()> {
        let mut config_path = home_dir().unwrap();
        config_path.push(".config/rusterino/settings.toml");
        let content = toml::to_string(&self)?;
        fs::write(config_path, content)?;
        Ok(())
    }
}
