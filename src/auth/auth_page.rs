use actix_web::{
    get, post,
    web::{self, Data},
    App, HttpResponse, HttpServer, Responder,
};
use anyhow::Result;
use dialoguer::{theme::ColorfulTheme, Input};
use futures::executor;
use std::sync::mpsc::Sender;
use tokio::sync::mpsc::channel;
use tokio::task;
use webbrowser;

#[get("/user-login")]
async fn user_login() -> impl Responder {
    let content = include_str!("./auth_page.html");

    HttpResponse::Ok()
        .append_header(("content-type", "text/html"))
        .body(content)
}

#[post("/stop-server")]
async fn stop_server(stopper: web::Data<Sender<()>>) -> HttpResponse {
    stopper.send(()).unwrap();
    HttpResponse::NoContent().finish()
}

pub fn open() -> Result<String> {
    // ask for port
    let port: u16 = Input::with_theme(&ColorfulTheme::default())
        .with_prompt("Enter port to serve login page on")
        .validate_with(|input: &String| -> Result<(), &str> {
            if input.parse::<u16>().is_ok() {
                Ok(())
            } else {
                Err("This is not a valid number")
            }
        })
        .default(8080.to_string()) // default port is 8080
        .interact()?
        .parse::<u16>()?;

    // define channel for server stop
    let (tx, mut rx) = channel::<()>(1);

    // define url and open in browser
    let url = &format!("http://localhost:{port}/user-login?p={port}", port = &port);
    webbrowser::open(url).unwrap();

    // start server
    let server = HttpServer::new(move || {
        App::new()
            .app_data(Data::new(tx.clone()))
            .service(user_login)
            .service(stop_server)
    })
    .bind(format!("127.0.0.1:{}", &port))?
    .run();

    // async wait for server stop to be called
    task::spawn(async move {
        rx.recv().await.unwrap();
        executor::block_on(server.stop(true))
    });

    // handle callback user data from twitch
    let user_string: String = Input::with_theme(&ColorfulTheme::default())
        .with_prompt("Paste login info here")
        .interact()?;
    Ok(user_string)
}
