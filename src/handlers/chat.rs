use super::message;
use super::message_history::MessageHistory;
use crate::settings::Settings;
use crate::twitch;
use crate::util::build_item;
use anyhow::Result;
use regex::Regex;
use std::collections::VecDeque;
use tui::widgets::ListItem;
use tui_input::Input;
use twitch_irc::login::StaticLoginCredentials;
use twitch_irc::message::ServerMessage;
use twitch_irc::{TwitchIRCClient, SecureTCPTransport};

#[derive(Clone)]
pub struct Chat {
    pub settings: Settings,
    pub messages: VecDeque<ListItem<'static>>,
    pub input: Input,
    pub message_history: MessageHistory,
    pub chatters: Vec<(String, ListItem<'static>)>,
    pub view: View,
    pub blocklist: Vec<(String, ListItem<'static>)>,
    pub win_width: usize,
    pub user_regex: Regex,
    pub irc_client: Option<TwitchIRCClient<SecureTCPTransport, StaticLoginCredentials>>,
}

pub const MESSAGE_CAPACITY: usize = 100;

#[derive(Clone, PartialEq)]
pub enum View {
    Normal,
    Chatters,
    ChangeChannel,
    _UserInfo(String),
    BlockWord,
    Help,
}

impl Default for Chat {
    fn default() -> Self {
        Chat {
            settings: Settings::read().unwrap(),
            messages: VecDeque::with_capacity(MESSAGE_CAPACITY),
            input: Input::default(),
            message_history: MessageHistory::default(),
            chatters: Vec::new(),
            view: View::Normal,
            blocklist: Vec::new(),
            win_width: 0,
            user_regex: Regex::new("").unwrap(),
            irc_client: None,
        }
    }
}

impl Chat {
    pub async fn new(
        settings: Settings,
        irc_client: TwitchIRCClient<SecureTCPTransport, StaticLoginCredentials>,
    ) -> Chat {
        let current_channel = settings.current_channel();

        let chatters = build_chatters_list(&current_channel).await;

        let mut blocklist: Vec<(String, ListItem<'static>)> = Vec::new();
        if let Some(blocked_words) = settings.blocked_words() {
            for word in blocked_words {
                blocklist.push((word.to_owned(), build_item::entry(word)));
            }
        }

        let regex_string = format!(r"(^|\s)@?{},?(\s|$)", settings.user_name());
        let user_regex = Regex::new(&regex_string).unwrap();

        irc_client.join(current_channel);

        Chat {
            settings,
            chatters,
            user_regex,
            irc_client: Some(irc_client),
            ..Default::default()
        }
    }

    pub fn handle_message(&mut self, msg: Box<ServerMessage>) -> Result<()> {
        match *msg {
            ServerMessage::Privmsg(m) => {
                for (word, _) in &self.blocklist {
                    if m.message_text.contains(word) {
                        return Ok(());
                    }
                }
                let data = message::privmsg(
                    m,
                    &self.settings.user_name(),
                    &self.user_regex,
                    &self.win_width
                );
                self.messages.push_front(data);
            }
            ServerMessage::Join(_) => {
                let data = message::join(&self.win_width);
                self.messages.push_front(data);
            }
            ServerMessage::ClearChat(m) => {
                let data = message::clear(m.action, &self.win_width);
                self.messages.push_front(data);
            }
            ServerMessage::RoomState(m) => {
                let data = message::roomstate(m, &self.win_width);
                self.messages.push_front(data);
            }
            _ => {}
        };
        Ok(())
    }

    pub async fn change_channel(&mut self, new_channel: String) -> Result<()> {
        let irc_client = self.irc_client.clone().unwrap();
        irc_client.part(self.settings.current_channel());
        irc_client.join(new_channel.clone());
        self.irc_client = Some(irc_client);
        self.settings.change_channel(new_channel.clone());
        self.messages = VecDeque::with_capacity(MESSAGE_CAPACITY);
        self.chatters = build_chatters_list(&new_channel).await;
        Ok(())
    }

    pub fn add_blocked_word(&mut self, word: String) {
        self.blocklist
            .push((word.to_owned(), build_item::entry(word.clone())));
        self.settings.add_blocked_word(word);
    }
}

async fn build_chatters_list(current_channel: &str) -> Vec<(String, ListItem<'static>)> {
    let mut chatters: Vec<(String, ListItem<'static>)> = Vec::new();

    let (count, chatters_map) = twitch::chatters(&current_channel).await.unwrap();

    let count_string = format!("chatter count: {}", &count);
    chatters.push(("+c".to_string(), build_item::entry(count_string)));
    chatters.push(("+e".to_string(), build_item::entry(String::default())));

    for category in chatters_map.into_iter() {
        let items = category.1.as_array().unwrap();
        if items.len() > 0 {
            chatters.push(("+h".to_string(), build_item::header(category.0)));
            for item in items {
                let item = item.as_str().unwrap_or_default().to_owned();
                chatters.push((item.clone(), build_item::entry(item)))
            }
            chatters.push(("+e".to_string(), build_item::entry(String::default())));
        }
    }

    chatters
}
