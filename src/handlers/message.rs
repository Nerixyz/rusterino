use crate::util::{build_item, color, time};
use regex::Regex;
use tui::{
    style::{Color, Style},
    text::Span,
    widgets::ListItem,
};
use twitch_irc::message::{ClearChatAction, FollowersOnlyMode, PrivmsgMessage, RoomStateMessage};

pub fn join(win_width: &usize) -> ListItem<'static> {
    build_item::message(
        time_stamp(),
        None,
        None,
        "connected".to_owned(),
        None,
        win_width - 2,
        None,
        system_info_style(),
    )
}

pub fn clear(action: ClearChatAction, win_width: &usize) -> ListItem<'static> {
    let msg = match action {
        ClearChatAction::ChatCleared => "Chat has been cleared by a moderator.".to_owned(),
        ClearChatAction::UserTimedOut {
            user_login,
            timeout_length,
            ..
        } => {
            let timeout = time::format(timeout_length);
            format!("{} has been timed out for {}.", &user_login, timeout)
        }
        ClearChatAction::UserBanned { user_login, .. } => {
            format!("{} has been permanently banned.", user_login)
        }
    };
    build_item::message(
        time_stamp(),
        None,
        None,
        msg,
        None,
        win_width - 2,
        None,
        system_info_style(),
    )
}

pub fn roomstate(msg: RoomStateMessage, win_width: &usize) -> ListItem<'static> {
    let msg = if let Some(toggle) = msg.emote_only {
        match toggle {
            true => "This room is now in emote-only mode.".to_owned(),
            false => "This room is no longer in emote-only mode.".to_owned(),
        }
    } else if let Some(toggle) = msg.follwers_only {
        match toggle {
            FollowersOnlyMode::Enabled(d) => {
                let time = time::format(d);
                format!("This room is now in {} followers-only mode.", time)
            }
            FollowersOnlyMode::Disabled => {
                "This room is no longer in followers-only mode.".to_owned()
            }
        }
    } else if let Some(toggle) = msg.r9k {
        match toggle {
            true => "This room is now in unique-chat mode.".to_owned(),
            false => "This room is no longer in unique-chat mode.".to_owned(),
        }
    } else if let Some(d) = msg.slow_mode {
        if d.as_secs() == 0 {
            "This room is no longer in slow mode.".to_owned()
        } else {
            let time = time::format(d);
            format!("This room is in {} slow mode.", time)
        }
    } else if let Some(toggle) = msg.subscribers_only {
        match toggle {
            true => "This room is now in subscribers-only mode.".to_owned(),
            false => "This room is no longer in subscribers-only mode.".to_owned(),
        }
    } else {unreachable!()};
    return build_item::message(
        time_stamp(),
        None,
        None,
        msg,
        None,
        win_width - 2,
        None,
        system_info_style(),
    )
}

pub fn privmsg(
    msg: PrivmsgMessage,
    user_name: &str,
    user_regex: &Regex,
    win_width: &usize,
) -> ListItem<'static> {
    let (c, name_style) = match &msg.name_color {
        Some(c) => {
            let c = color::correct(c);
            (c, Style::default().fg(c))
        }
        None => (Color::White, Style::default().fg(Color::Rgb(150, 150, 150))),
    };

    // msg highlighting on @
    let user_name = user_name.to_lowercase();
    let mut text_style = Style::default();
    if user_regex.is_match(&msg.message_text.to_lowercase())
        && msg.sender.name.to_lowercase() != user_name
    {
        text_style = text_style.bg(Color::Rgb(87, 15, 58))
    }

    let mut badges_collection: Vec<&str> = Vec::new();
    for badge in msg.badges.iter() {
        let badge = match badge.name.as_str() {
            "broadcaster" => "🟥",
            "moderator" => "🟩",
            "vip" => "🔷",
            "subscriber" => "🌟",
            "founder" => "🌠",
            "staff" => "🔧",
            _ => "",
        };
        badges_collection.push(badge);
    }

    let mut splitter = String::from(": ");

    if msg.is_action {
        text_style = text_style.fg(c);
        splitter = String::from(" ");
    }

    return build_item::message(
        time_stamp(),
        Some(badges_collection.join("")),
        Some(msg.sender.name),
        msg.message_text.clone(),
        Some(splitter),
        win_width - 2,
        Some(name_style),
        text_style,
    );
}

fn time_stamp() -> Span<'static> {
    Span::styled(time::now(), system_info_style())
}

fn system_info_style() -> Style {
    Style::default().fg(Color::Rgb(120, 120, 120))
}
