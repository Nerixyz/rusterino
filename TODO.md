# TODO
https://dev.twitch.tv/docs/irc/guide
https://dev.twitch.tv/docs/api/reference
- [X] send message
- [X] /me
- [X] line wrap on too long messages
- [ ] tab completion emotes
- [X] tab completion chatters https://tmi.twitch.tv/group/user/m0xyy/chatters
- [X] fix reading own message contents
- [X] message timestamps
- [X] message history
- [X] input navigation and editing (navigate with left arrow and right arrow)
- [ ] fix the weird [?] character that pops up sometimes
- [X] CTRL+Enter keybind (input sequence not supported, using ALT+Enter instead for now)
- [ ] verified badge
- [ ] performance (could be a pog learning moment)
- [X] highlight mentions

## IRC message types
- [X] handle roomstate (emote only, follower only, subs only, slow mode Xs)
- [ ] handle usernotice (sub, sub gifts and more)

## Views:
- [X] change channel view
- [X] block word view (regex blocking?)
- [ ] command handling view (chatterino like commands)
- [X] chatters view
- [ ] user info view
